package piece;
/**
 * 
 * @author chrislopresti
 * @author kylemyers
 *
 */
public class hash extends piece{
	
	public String id;

	/**
	 * 
	 * @param col
	 * @param row
	 * @param color
	 * @param name
	 * @param id
	 * 
	 * constructor for the hash with the parameters as its attributes
	 * calls super constructor of piece class
	 */
	public hash(int col, int row, String color, String name , String id) {
		super(col, row, color, name, id);

	}

}
