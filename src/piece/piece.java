package piece;

/**
 *  
 * @author chrislopresti
 * @author kylemyers
 *
 */
public class piece implements move, check  {
	
	public String id;
	
	public String name;
	public int column;
	public int rows;
	public String color;
	public boolean moved;
	public boolean justEnpassanted;


	/**
	 * 
	 * @param col
	 * @param row
	 * @param color
	 * @param name
	 * @param id
	 * 
	 * piece constructor
	 */
	public piece(int col, int row, String color, String name, String id) {
		this.column = col;
		this. rows = row;
		this.color = color;
		this.moved = false;
		this.name = name;
		this.id = id;
		
		
	}


	/**
	 * Method to move the current piece, will have specification based on the pieces legal moves
	 * 
	 * @param col
	 * @param row
	 * @param board
	 * 
	 * Input: the row, col of the desired move and the board of piece objects
	 */
	public boolean move(int col, int row, piece[][] board) {
		// TODO Auto-generated method stub
		return false;
	}


	/**
	 * method to check if the current piece is putting the opposing king in check
	 * 
	 * @param board
	 */
	public boolean check(piece[][] board) {
		// TODO Auto-generated method stub
		
		return false;
		
	}


	
	
	


}
