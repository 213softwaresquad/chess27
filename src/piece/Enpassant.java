package piece;
/**
 * 
 * @author chrislopresti
 * @author kylemyers
 *
 */
public class Enpassant extends piece {

	/**
	 * 
	 * @param col
	 * @param row
	 * @param color
	 * @param name
	 * @param id
	 * 
	 * constructor for the enpassant object with the parameters as its attributes
	 * calls super constructor of piece class
	 */
	public Enpassant(int col, int row, String color, String name, String id) {
		super(col, row, color, name, id);
		// TODO Auto-generated constructor stub
	}

}
