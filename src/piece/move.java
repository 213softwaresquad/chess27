package piece;
/**
 * 
 * @author chrislopresti
 * @author kylemyers
 *
 */
public interface move {

	public boolean move(int col, int row, piece[][] board);
	
}
