package piece;
/**
 * 
 * @author chrislopresti
 * @author kylemyers
 *
 */
public class bishop extends piece {

	public String id;

	/**
	 * 
	 * @param col
	 * @param row
	 * @param color
	 * @param name
	 * @param id
	 * 
	 * constructor for the bishop with the parameters as its attributes
	 * calls super constructor of piece class
	 */
	public bishop(int col, int row, String color, String name, String id) {
		super(col, row, color, name, id);

	}

	/**
	 * Method to see if the move is allowed on a bishop
	 * Takes in the board and desired position of the piece
	 * 
	 * @param col
	 * @param row
	 * @param board
	 * @return boolean 
	 */
	public boolean move(int col, int row, piece[][] board) {

		// check to make sure the column and row have changed
		if (col == column || row == rows) {
			return false;
		}

		if ((col != column && row != rows)) {

			int colDiff;
			int rowDiff;

			// check for move down to the left
			if ((col < column) && (row < rows)) {
				colDiff = column - col;
				rowDiff = rows - row;

				if (colDiff != rowDiff) {
					return false;
				}
				this.rows = row;
				this.column = col;
				this.moved = true;
				board[0][0].name.equals("false");
				return true;
			}

			// check for move down to the right
			if ((col > column) && (row < rows)) {
				colDiff = col - column;
				rowDiff = rows - row;

				if (colDiff != rowDiff) {
					return false;
				}
				this.rows = row;
				this.column = col;
				this.moved = true;
				board[0][0].name.equals("false");
				return true;
			}

			// check for move up to the right
			if ((col > column) && (row > rows)) {
				colDiff = col - column;
				rowDiff = row - rows;

				if (colDiff != rowDiff) {
					return false;
				}
				this.rows = row;
				this.column = col;
				this.moved = true;
				board[0][0].name.equals("false");
				return true;
			}

			// check for move up to the left
			if ((col < column) && (row > rows)) {
				colDiff = column - col;
				rowDiff = row - rows;

				if (colDiff != rowDiff) {
					return false;
				}
				this.rows = row;
				this.column = col;
				this.moved = true;
				board[0][0].name.equals("false");
				return true;
			}

		}

		return false;
	}
	
	/**
	 * Method to check to see if the bishop is putting the opposing king in check
	 * Input the board of pieces
	 * 
	 * @param board
	 * @return boolean
	 */
	public boolean check(piece[][] board) {

		// check down to the left diagonal
		int place = 0;

		if (rows - 1 != 0) {
			place = column - 1;

			for (int i = rows - 1; i > 0; i--) {

				if (place >0 && (board[i][place].id.equals("hash") || board[i][place].id.equals("space"))) {

					place--;

					continue;

				} else if (place >0 &&board[i][place].id.equals("king") && !(board[i][place].color.equals(this.color))) {

					board[0][0].name.equals("false");
					
					return true;

				} else {

					break;

				}

			}
		}
		// System.out.println("valid move");
		// checks for bottom right diagonal
		if (rows - 1 != 0) {
			place = column + 1;

			for (int i = rows - 1; i > 0; i--) {

				if (place <9 && (board[i][place].id.equals("hash") || board[i][place].id.equals("space"))) {

					place++;

					continue;

				} else if (place <9 && board[i][place].id.equals("king") && !(board[i][place].color.equals(this.color))) {

					board[0][0].name.equals("false");
					
					return true;

				} else {

					break;

				}

			}
		}
		// System.out.println("valid move");
		// check upper left diagonal
		if (rows + 1 != 9) {
			place = column - 1;

			for (int i = rows + 1; i < 9; i++) {

				if ( place >0 && (board[i][place].id.equals("hash") || board[i][place].id.equals("space"))) {

					place--;

					continue;

				} else if (place >0 &&board[i][place].id.equals("king") && !(board[i][place].color.equals(this.color))) {

					board[0][0].name.equals("false");
					
					return true;

				} else {

					break;

				}

			}
		}
		// System.out.println("valid move");
		// check upper right diagonal
		if (rows + 1 != 9) {
			place = column + 1;

			for (int i = rows + 1; i < 9; i++) {

				if (place <9 &&(board[i][place].id.equals("hash") || board[i][place].id.equals("space"))) {

					place++;

					continue;

				} else if (place <9 && board[i][place].id.equals("king") && !(board[i][place].color.equals(this.color))) {

					board[0][0].name.equals("false");
					
					return true;

				} else {

					break;

				}

			}
		}
		// System.out.println("valid move");

		return false;

	}

}
